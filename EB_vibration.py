import numpy as np
import math
import matplotlib.pyplot as plt
from applybc import bcapply
from applybc_vib import bcapply1
from definebc import definebcs as defbc
from sympy import *
# from decimal import Decimal
from scipy import linalg
import time

start=time.time()



##Number of the element
part=20
ii=1000       ## Penalty parametre
l =.12        ##length
b=.03
h=.002
Ar=b*h         ## Area
rho=2700       ##Density
E=70*(10**(9)) ## Youngs modulus         ##
I=b*(h**3)/12  ## Moment of inertia
# I=100*(10**(-12))
# nu=.3
s=l          ##Parametre
# mid=int((2*(part-1))/2)
##Connectivity
connec1=[[0,1]]
for i in range(1,part):
    connec1.append([i,i-1,i+1])
connec1.append([part,part-1])
print('connec1',connec1)

ee=l/part                    ##Distance between points
p=np.linspace(0,l,part+1)    ##point coordinates
nop=len(p)                   ##No. of points
# print(nop)
bound_coord=[0]              ##BOUNDARY COORDINATES
for i in range(0,len(p)-1):
    bound_coord.append(float(format(p[i] + (ee / 2), '.10')))
bound_coord.append(l)
print('boundary coordinate',bound_coord)

##LENGTH OF ELEMENTS
el_len=[]
for i in range(0,len(bound_coord)-1):
    el_len.append(float(format(bound_coord[i+1]-bound_coord[i],'.10')))

print('Element length',el_len)



## Cubic Radial Basis Functions and its derivatives
def Qt(xi, xj):
    Rij= (abs(xi-xj)/s)**3
    return Rij

def Qtx(xi, xj):
    Rijx=(3/s)*(abs(xi-xj)/s)**2
    return Rijx

def Qtxx(xi, xj):
    Rijxx=(6/s**2)*(abs(xi-xj)/s)
    return Rijxx
## Polynomial Basis Functions and its derivatives
def Pt(x):
    Sij=np.array([[1,x,x**2]])
    return Sij

def Ptx(x):
    Sijx=np.array([[0,1,2*x]])
    return Sijx

## Shape functions
def shpfC(x,connec_now,d):
    l_size = len(connec_now)
    Rm = np.zeros((2 * l_size, 2 * l_size), dtype=float)
    for i in range(l_size):
        for j in range(l_size):
            # print(j)
            xj = x[j]
            xi = x[i]
            Rm[2 * j][2 * i] = Qt(xi, xj)
            Rm[2 * j][(2 * i) + 1] = Qtx(xi, xj)
            Rm[(2 * j) + 1][2 * i] = Qtx(xi, xj)
            Rm[(2 * j) + 1][(2 * i) + 1] = Qtxx(xi, xj)
    # print(Rm)
    Sq = np.zeros((2*l_size, 3), dtype=float)
    for l in range(l_size):
        for m in range(3):
            xi = x[l]
            # print(Smat(xi))
            # print(Sq)
            Sq[2 * l][m] = Pt(xi)[0][m]
            Sq[(2 * l) + 1][m] = Ptx(xi)[0][m]
    # print(Sq)
    G1 = np.concatenate((Rm, Sq), axis=1)
    G2 = np.concatenate((np.transpose(Sq), np.zeros((3, 3), dtype=float)), axis=1)
    G = np.concatenate((G1, G2), axis=0)
    # print("GGG",G)
    if l_size == 3:
        x0 = x[0]
        x1 = x[1]
        x2 = x[2]
        X = symbols('X')
        Rt = [((X-x0)/s)**3,(3/s)*((X-x0)/s)**2,
              ((X-x1)/s)**3,(3/s)*((X-x1)/s)**2,
              -((X-x2)/s)**3,(3/s)*((X-x2)/s)**2]
        St = [1, X,X**2]
        Fx=[(3/s)*((X-x0)/s)**2,(6/s**2)*((X-x0)/s),
            (3/s)*((X-x1)/s)**2,(6/s**2)*((X-x1)/s),
            (3/s)*((X-x2)/s)**2,-(6/s**2)*((X-x2)/s)]
        Fy = [diff(item, X) for item in St]
        Fxx=[(6/s**2)*((X-x0)/s),(6/s**3),
             (6/s**2)*((X-x1)/s),(6/s**3),
             -(6/s**2)*((X-x2)/s),(6/s**3)]
        Fyy = [diff(item, X) for item in Fy]
        Fxxx = [(6/s**3),0,(6/s**3),0,(6/s**3),0]
        Fyyy = [diff(item, X) for item in Fyy]
        rt = np.array([Rt])
        st = np.array([St])
        Rtx = np.array([Fx])
        # print(Rtx)
        Stx = np.array([Fy])
        Rtxx = np.array([Fxx])
        # print(Rtxx)
        Stxx = np.array([Fyy])
        Rtxxx = np.array([Fxxx])
        Stxxx = np.array([Fyyy])
        R_S = np.concatenate((Rtx, Stx), axis=1)
        p_s = np.concatenate((rt, st), axis=1)
        R_S2 = np.concatenate((Rtxx, Stxx), axis=1)
        R_S3 = np.concatenate((Rtxxx, Stxxx), axis=1)


    if l_size == 2 and connec_now[0]<connec_now[1]:
        x0 = x[0]
        x1 = x[1]
        X = symbols('X')
        Rt = [((X - x0) / s) ** 3,(3/s)* ((X - x0) / s) ** 2,
              -((X - x1)/ s) ** 3,(3/s)* ((X - x1) / s) ** 2]
        St =[1, X,X**2]
        Fx = [(3/s) * ((X - x0) / s) ** 2, (6/s**2)* ((X - x0) / s),
              (3/s) * ((X - x1) / s) ** 2, -(6/s**2) * ((X - x1)/ s)]
        Fy = [diff(item, X) for item in St]
        Fxx = [(6/s**2) * ((X - x0) / s), (6/s**3),
               -(6/s**2) * ((X - x1) / s),(6/s**3)]

        Fyy = [diff(item, X) for item in Fy]
        Fxxx = [(6/s**3) , 0,(6/s**3), 0]
        Fyyy = [diff(item, X) for item in Fyy]

        rt = np.array([Rt])
        st = np.array([St])
        Rtx = np.array([Fx])
        Stx = np.array([Fy])
        Rtxx = np.array([Fxx])
        Stxx = np.array([Fyy])
        Rtxxx = np.array([Fxxx])
        Stxxx = np.array([Fyyy])
        R_S = np.concatenate((Rtx, Stx), axis=1)
        p_s = np.concatenate((rt, st), axis=1)
        R_S2 = np.concatenate((Rtxx, Stxx), axis=1)
        R_S3 = np.concatenate((Rtxxx, Stxxx), axis=1)

    if l_size == 2 and connec_now[0]>connec_now[1]:
        x0 = x[0]
        x1 = x[1]
        X = symbols('X')
        Rt = [((X - x0) / s) ** 3, (3/s) * ((X - x0) / s) ** 2,
              ((X - x1) / s) ** 3,(3/s)* ((X - x1) / s) ** 2]
        St =[1, X,X**2]
        Fx = [(3/s) * ((X - x0) / s) ** 2, (6/s**2)* -((X - x0) / s),
              (3/s) * ((X - x1) / s) ** 2,(6/s**2) * ((X - x1) / s)]
        Fy = [diff(item, X) for item in St]
        Fxx = [(6/s**2) * -((X - x0) / s), (6/s**3),
               (6/s**2) * ((X - x1) / s), (6/s**3)]

        Fyy = [diff(item, X) for item in Fy]
        Fxxx = [(6/s**3), 0, (6/s**3), 0]
        Fyyy = [diff(item, X) for item in Fyy]

        rt = np.array([Rt])
        st = np.array([St])
        Rtx = np.array([Fx])
        Stx = np.array([Fy])
        Rtxx = np.array([Fxx])
        Stxx = np.array([Fyy])
        Rtxxx = np.array([Fxxx])
        Stxxx = np.array([Fyyy])
        R_S = np.concatenate((Rtx, Stx), axis=1)
        p_s = np.concatenate((rt, st), axis=1)
        R_S2 = np.concatenate((Rtxx, Stxx), axis=1)
        R_S3 = np.concatenate((Rtxxx, Stxxx), axis=1)

    C0 = np.dot(R_S, np.linalg.inv(G))
    c0 = np.dot(p_s, np.linalg.inv(G))
    C00 = np.dot(R_S2, np.linalg.inv(G))
    C000 = np.dot(R_S3, np.linalg.inv(G))
    # print(C0)
    # print('C000',C000)
    # print(np.shape(C0), np.shape(R_S))
    I1 = np.zeros((3, 2 * l_size), dtype=float)
    I2 = np.concatenate((np.identity(2 * l_size), I1), axis=0)
    # print(I2)
    c = np.dot(C0, I2)  ##FIRST DERIVATIVE U
    p_f = np.dot(c0, I2)  ##FIRST U
    # print(p_f)
    cc = np.dot(C00, I2)  ##SECOND DERIVATIVE U
    ccc = np.dot(C000, I2)
    # print('ccc',ccc)
    # print('-----------------------------------------')
    r1, c1 = np.shape(c)
    # print(c)
    for a in range(r1):
        for b in range(c1):
            c[a][b] = c[a][b].subs(X, d)
           
    r3, c3 = np.shape(p_f)
    for a in range(r3):
        for b in range(c3):
            p_f[a][b] = p_f[a][b].subs(X, d)

    r4, c4 = np.shape(cc)
    for a in range(r4):
        for b in range(c4):
            cc[a][b] = cc[a][b].subs(X, d)
    r5, c5 = np.shape(ccc)
    for a in range(r5):
        for b in range(c5):
            ccc[a][b] = ccc[a][b].subs(X, d)
    return c, p_f, cc,ccc

def shpfC1(x,connec_now,d):
    l_size = len(connec_now)
    Rm = np.zeros((2 * l_size, 2 * l_size), dtype=float)
    for i in range(l_size):
        for j in range(l_size):
            # print(j)
            xj = x[j]
            xi = x[i]
            Rm[2 * j][2 * i] = Qt(xi, xj)
            Rm[2 * j][(2 * i) + 1] = Qtx(xi, xj)
            Rm[(2 * j) + 1][2 * i] = Qtx(xi, xj)
            Rm[(2 * j) + 1][(2 * i) + 1] = Qtxx(xi, xj)
    # print(Rm)
    Sq = np.zeros((2*l_size, 3), dtype=float)
    for l in range(l_size):
        for m in range(3):
            xi = x[l]
            Sq[2 * l][m] = Pt(xi)[0][m]
            Sq[(2 * l) + 1][m] = Ptx(xi)[0][m]
    # print(Sq)
    G1 = np.concatenate((Rm, Sq), axis=1)
    G2 = np.concatenate((np.transpose(Sq), np.zeros((3, 3), dtype=float)), axis=1)
    G = np.concatenate((G1, G2), axis=0)
    # print("GGG",G)
    if l_size == 3:
        x0 = x[0]
        x1 = x[1]
        x2 = x[2]
        X = symbols('X')
        Rt = [-((X-x0)/s)**3,(3/s)*((X-x0)/s)**2,
              ((X-x1)/s)**3,(3/s)*((X-x1)/s)**2,
              -((X-x2)/s)**3,(3/s)*((X-x2)/s)**2]
        St = [1, X,X**2]
        Fx=[(3/s)*((X-x0)/s)**2,(6/s**2)*-((X-x0)/s),
            (3/s)*((X-x1)/s)**2,(6/s**2)*((X-x1)/s),
            (3/s)*((X-x2)/s)**2,-(6/s**2)*((X-x2)/s)]
        Fy = [diff(item, X) for item in St]
        Fxx=[(6/s**2)*-((X-x0)/s),(6/s**3),
             (6/s**2)*((X-x1)/s),(6/s**3),
             -(6/s**2)*((X-x2)/s),(6/s**3)]
        Fyy = [diff(item, X) for item in Fy]
        Fxxx = [(6/s**3),0,(6/s**3),0,(6/s**3),0]
        Fyyy = [diff(item, X) for item in Fyy]
        rt = np.array([Rt])
        st = np.array([St])
        Rtx = np.array([Fx])
        # print(Rtx)
        Stx = np.array([Fy])
        Rtxx = np.array([Fxx])
        # print(Rtxx)
        Stxx = np.array([Fyy])
        Rtxxx = np.array([Fxxx])
        Stxxx = np.array([Fyyy])
        R_S = np.concatenate((Rtx, Stx), axis=1)
        p_s = np.concatenate((rt, st), axis=1)
        R_S2 = np.concatenate((Rtxx, Stxx), axis=1)
        R_S3 = np.concatenate((Rtxxx, Stxxx), axis=1)


    if l_size == 2 and connec_now[0]<connec_now[1]:
        x0 = x[0]
        x1 = x[1]
        X = symbols('X')
        Rt = [((X - x0) / s) ** 3,(3/s)* ((X - x0) / s) ** 2,
              -((X - x1) / s) ** 3,(3/s)* ((X - x1) / s) ** 2]
        St =[1, X,X**2]
        Fx = [(3/s) * ((X - x0) / s) ** 2, (6/s**2)* ((X - x0) / s),
              (3/s) * ((X - x1) / s) ** 2, -(6/s**2) * ((X - x1) / s)]
        Fy = [diff(item, X) for item in St]
        Fxx = [(6/s**2) * ((X - x0) / s), (6/s**3),
               -(6/s**2) * ((X - x1) / s),(6/s**3)]

        Fyy = [diff(item, X) for item in Fy]
        Fxxx = [(6/s**3) , 0,(6/s**3), 0]
        Fyyy = [diff(item, X) for item in Fyy]

        rt = np.array([Rt])
        st = np.array([St])
        Rtx = np.array([Fx])
        Stx = np.array([Fy])
        Rtxx = np.array([Fxx])
        Stxx = np.array([Fyy])
        Rtxxx = np.array([Fxxx])
        Stxxx = np.array([Fyyy])
        R_S = np.concatenate((Rtx, Stx), axis=1)
        p_s = np.concatenate((rt, st), axis=1)
        R_S2 = np.concatenate((Rtxx, Stxx), axis=1)
        R_S3 = np.concatenate((Rtxxx, Stxxx), axis=1)

    if l_size == 2 and connec_now[0]>connec_now[1]:
        x0 = x[0]
        x1 = x[1]
        X = symbols('X')
        Rt = [-((X - x0) / s) ** 3,(3/s)* ((X - x0) / s) ** 2,
              ((X - x1) / s) ** 3, (3/s) * ((X - x1) / s) ** 2]
        St =[1, X,X**2]
        Fx = [(3/s) * ((X - x0) / s) ** 2,(6/s**2) * -((X - x0) / s),
              (3 / s)* ((X - x1) / s) ** 2,(6/s**2) * ((X - x1) / s)]
        Fy = [diff(item, X) for item in St]
        Fxx = [(6/s**2)* -((X - x0) / s), (6/s**3),
               (6/s**2) * ((X - x1) / s),(6/s**3)]

        Fyy = [diff(item, X) for item in Fy]
        Fxxx = [(6/s**3), 0, (6/s**3), 0]
        Fyyy = [diff(item, X) for item in Fyy]

        rt = np.array([Rt])
        st = np.array([St])
        Rtx = np.array([Fx])
        Stx = np.array([Fy])
        Rtxx = np.array([Fxx])
        Stxx = np.array([Fyy])
        Rtxxx = np.array([Fxxx])
        Stxxx = np.array([Fyyy])
        R_S = np.concatenate((Rtx, Stx), axis=1)
        p_s = np.concatenate((rt, st), axis=1)
        R_S2 = np.concatenate((Rtxx, Stxx), axis=1)
        R_S3 = np.concatenate((Rtxxx, Stxxx), axis=1)

    x0 = x[0]
    C0 = np.dot(R_S, np.linalg.inv(G))
    c0 = np.dot(p_s, np.linalg.inv(G))
    C00 = np.dot(R_S2, np.linalg.inv(G))
    C000 = np.dot(R_S3, np.linalg.inv(G))
    I1 = np.zeros((3, 2 * l_size), dtype=float)
    I2 = np.concatenate((np.identity(2 * l_size), I1), axis=0)
    # print(I2)
    c = np.dot(C0, I2)  ##FIRST DERIVATIVE U
    p_f = np.dot(c0, I2)  ##FIRST U
    # print('-----',p_f)
    cc = np.dot(C00, I2)  ##SECOND DERIVATIVE U
    ccc = np.dot(C000, I2)
    # print('ccc',ccc)
    # print('-----------------------------------------')
    r1, c1 = np.shape(c)
    # print(c)
    for a in range(r1):
        for b in range(c1):
            c[a][b] = c[a][b].subs(X, d)
            # print(c[a][b])
    # print(c)

    r3, c3 = np.shape(p_f)
    for a in range(r3):
        for b in range(c3):
            p_f[a][b] = p_f[a][b].subs(X, d)
    # print(p_f)

    r4, c4 = np.shape(cc)
    for a in range(r4):
        for b in range(c4):
            cc[a][b] = cc[a][b].subs(X, d)
            # print(cc[a][b])
    # print(cc)
    r5, c5 = np.shape(ccc)
    for a in range(r5):
        for b in range(c5):
            ccc[a][b] = ccc[a][b].subs(X, d)
            # print(cc[a][b])
    # print(ccc)

    return c, p_f, cc,ccc

# float(format(bound_coord[i+1]-bound_coord[i],'.10'))

## Coordinate values
coord_list = []
for i in connec1:
    coordd = []
    for j in i:
        coordd.append(p[j])
    coord_list.append(coordd)
print(coord_list)

CXX = []
for k in range(len(connec1)):
    x = coord_list[k]
    connec = connec1[k]
    val = x[0]
    c1xx = shpfC(x, connec, val)[2]
    CXX.append(c1xx)

# print(CXX)

CX = []
for k in range(len(connec1)):
    x = coord_list[k]
    connec = connec1[k]
    val = x[0]
    c1x = shpfC(x, connec, val)[0]
    CX.append(c1x)

# print(CX)

C = []
for k in range(len(connec1)):
    x = coord_list[k]
    connec = connec1[k]
    val = x[0]
    c1 = shpfC(x, connec, val)[1]
    C.append(c1)

print(C)

CXXX = []
for k in range(len(connec1)):
    x = coord_list[k]
    connec = connec1[k]
    val = x[0]
    c1 = shpfC(x, connec, val)[3]
    CXXX.append(c1)

# print('CXXX',CXXX)

## Point-stiffness Matrix calculation
def pointstiff(B,l):
    K=[]
    for i in range(len(B)):
        Bt=np.transpose(np.array(B[i]))
        K.append(E*I*np.dot(Bt,np.array(B[i]))*l[i])
    return K


## Mass-stiffness Matrix calculation
def massmatrix(B,l):
    K=[]
    for i in range(len(B)):
        Bt=np.transpose(np.array(B[i]))
        K.append(rho*Ar*np.dot(Bt,np.array(B[i]))*l[i])
    return K


K_all=pointstiff(CXX,el_len)
# print(K_all)
M_all=massmatrix(C,el_len)
# print(M_all)


## Boundary-stiffness Matrix calculation
# h=1
conn_h=[]
Kh_a = []
for i in connec1:
    for jj in range(1,len(i)):
        conn=list(set(connec1[i[0]]).union(set(connec1[i[jj]])))
        if conn not in conn_h:
            h = el_len[min(i[0],i[jj])]
            # print((ii / h))
            # print(Decimal(ii)/Decimal(h))
            conn_h.append(conn)
            n1=shpfC(coord_list[i[0]],i,bound_coord[max(i[0],i[jj])])[1]
            # print(n1)
            N1=np.zeros((1,2*len(conn)),dtype=float)
            for k in range(1):
                aa=0
                for m in connec1[i[0]]:
                    mm=conn.index(m)
                    N1[k][2*mm]=N1[k][2*mm]+n1[k][2*aa]
                    N1[k][2 * mm+1] = N1[k][2 * mm+1] + n1[k][2 * aa+1]
                    aa=aa+1
            # print('N1',N1)
            # print(i)
            n2=shpfC1(coord_list[i[jj]],connec1[i[jj]],bound_coord[max(i[0],i[jj])])[1]
            # print(n2)
            N2 = np.zeros((1, 2*len(conn)), dtype=float)

            for k in range(1):
                bb = 0
                for m in connec1[i[jj]]:
                    mm = conn.index(m)
                    N2[k][2 * mm] = N2[k][2 * mm] + n2[k][2 * bb]
                    N2[k][2 * mm + 1] = N2[k][2 * mm + 1] +n2[k][2 * bb + 1]
                    bb = bb + 1
            # print('N2',N2)
            # print(i)

            b1 = shpfC(coord_list[i[0]], i, bound_coord[max(i[0], i[jj])])[0]
            # print(b1)
            B1 = np.zeros((1, 2*len(conn)), dtype=float)
            for k in range(1):
                aa = 0
                for m in connec1[i[0]]:
                    mm = conn.index(m)
                    B1[k][2 * mm] = B1[k][2 * mm] + b1[k][2 * aa]
                    B1[k][2 * mm + 1] = B1[k][2 * mm + 1] + b1[k][2 * aa + 1]
                    aa = aa + 1
            print('B1',B1)
            # print(i)
            b2 = shpfC1(coord_list[i[jj]], connec1[i[jj]], bound_coord[max(i[0], i[jj])])[0]
            # print(b2)
            B2 = np.zeros((1, 2*len(conn)), dtype=float)

            for k in range(1):
                bb = 0
                for m in connec1[i[jj]]:
                    mm = conn.index(m)
                    B2[k][2 * mm] = B2[k][2 * mm] + b2[k][2 * bb]
                    B2[k][2 * mm + 1] = B2[k][2 * mm + 1] + b2[k][2 * bb + 1]
                    bb = bb + 1
            print('B2',B2)
            # print(i)
            c1 = shpfC(coord_list[i[0]], i, bound_coord[max(i[0], i[jj])])[2]
            # print(c1)
            C1 = np.zeros((1, 2*len(conn)), dtype=float)
            for k in range(1):
                aa = 0
                for m in connec1[i[0]]:
                    mm = conn.index(m)
                    C1[k][2 * mm] = C1[k][2 * mm] + c1[k][2 * aa]
                    C1[k][2 * mm + 1] = C1[k][2 * mm + 1] + c1[k][2 * aa + 1]
                    aa = aa + 1
            # print('C1',C1)
            # print(i)
            c2 = shpfC1(coord_list[i[jj]], connec1[i[jj]], bound_coord[max(i[0], i[jj])])[2]
            # print(c2)
            C2 = np.zeros((1, 2*len(conn)), dtype=float)

            for k in range(1):
                bb = 0
                for m in connec1[i[jj]]:
                    mm = conn.index(m)
                    C2[k][2 * mm] = C2[k][2 * mm] + c2[k][2 * bb]
                    C2[k][2 * mm + 1] = C2[k][2 * mm + 1] + c2[k][2 * bb + 1]
                    bb = bb + 1
            # print('C2',C2)
            # print(i)

            a1 = shpfC(coord_list[i[0]], i, bound_coord[max(i[0], i[jj])])[3]
            # print(a1)
            A1 = np.zeros((1, 2*len(conn)), dtype=float)
            for k in range(1):
                aa = 0
                for m in connec1[i[0]]:
                    mm = conn.index(m)
                    A1[k][2 * mm] = A1[k][2 * mm] + a1[k][2 * aa]
                    A1[k][2 * mm + 1] = A1[k][2 * mm + 1] + a1[k][2 * aa + 1]
                    aa = aa + 1
            # print('A1',A1)
            # print(i)
            a2 = shpfC1(coord_list[i[jj]], connec1[i[jj]], bound_coord[max(i[0], i[jj])])[3]
            # print(a2)
            A2 = np.zeros((1, 2*len(conn)), dtype=float)

            for k in range(1):
                bb = 0
                for m in connec1[i[jj]]:
                    mm = conn.index(m)
                    A2[k][2*mm] = A2[k][2*mm] + a2[k][2*bb]
                    A2[k][2 * mm+1] = A2[k][2 * mm+1] + a2[k][2 * bb+1]
                    bb = bb + 1
            # print('A2',A2)
            # print(i)

            kh1 = (.5 * (E*I*np.dot(np.transpose(A1), N1) - E*I* np.dot(np.transpose(C1), B1) -E*I*np.dot(np.transpose(B1), C1)) + E*I*(ii / h) *(np.dot(np.transpose(B1), B1)-np.dot(np.transpose(N1), N1)))
            kh2 = (.5 * (-E*I*np.dot(np.transpose(A1), N2) + E*I* np.dot(np.transpose(C1), B2) -E*I*np.dot(np.transpose(B1), C2)) + E*I*(ii / h) *(-np.dot(np.transpose(B1), B2)+np.dot(np.transpose(N1), N2)))
            kh3 = (.5 * (E*I*np.dot(np.transpose(A2), N1) - E*I* np.dot(np.transpose(C2), B1) +E*I*np.dot(np.transpose(B2), C1)) + E*I*(ii / h) *(-np.dot(np.transpose(B2), B1)+np.dot(np.transpose(N2), N1)))
            kh4 = (.5 * (-E*I*np.dot(np.transpose(A2), N2) + E*I* np.dot(np.transpose(C2), B2) +E*I*np.dot(np.transpose(B2), C2)) + E*I*(ii / h) *(np.dot(np.transpose(B2), B2)-np.dot(np.transpose(N2), N2)))
            kh = kh1 + kh2 + kh3 + kh4
            # print(kh)
            Kh_a.append(kh)
        else:
            continue
# print('KH_a',Kh_a)

#
#
K_P=np.zeros((2*len(connec1),2*len(connec1)),dtype=float)
for e in range(len(connec1)):
    kp=K_all[e]
    # print(kp)
    for i in range(len(connec1[e])):
        rp=connec1[e][i]
        # print(rp)
        for j in range(len(connec1[e])):
            cp=connec1[e][j]
            # print(cp)
            K_P[2*rp][2*cp]=K_P[2*rp][2*cp]+kp[2*i][2*j]
            K_P[2 * rp][2 * cp+1] = K_P[2 * rp][2*cp+1] + kp[2 * i][2 * j+1]
            K_P[2 * rp+1][2 * cp] = K_P[2 * rp+1][2 * cp] + kp[2 * i+1][2 * j]
            K_P[2 * rp+1][2 * cp+1] = K_P[2 * rp+1][2*cp+1] + kp[2 * i+1][2 * j+1]
            # print(K_P)
    # print('----------------------------------------------------')

# print(K_P)


##ASSEMBLY OF BOUNDARYSTIFFNESS MATRICES

K_B=np.zeros((2*len(connec1),2*len(connec1)),dtype=float)
for e in range(len(conn_h)):
    kph=Kh_a[e]
    # print(kph)
    for i in range(len(conn_h[e])):
        rb=conn_h[e][i]
        for j in range(len(conn_h[e])):
            cb=conn_h[e][j]
            K_B[2 * rb][2 * cb] = K_B[2 * rb][2 * cb] + kph[2 * i][2 * j]
            K_B[2 * rb][2 * cb + 1] = K_B[2 * rb][2 * cb + 1] + kph[2 * i][2 * j + 1]
            K_B[2 * rb + 1][2 * cb] = K_B[2 * rb + 1][2 * cb] + kph[2 * i + 1][2 * j]
            K_B[2 * rb + 1][2 * cb + 1] = K_B[2 * rb + 1][2 * cb + 1] + kph[2 * i + 1][2 * j + 1]
    # print(K_B)
    # print('----------------------------------------------------')
# print(K_B)


#
Kg=K_P+K_B
M_P=np.zeros((2*len(connec1),2*len(connec1)),dtype=float)
for e in range(len(connec1)):
    mp=M_all[e]
    # print(mp)
    for i in range(len(connec1[e])):
        rp=connec1[e][i]
        # print(rp)
        for j in range(len(connec1[e])):
            cp=connec1[e][j]
            # print(cp)
            M_P[2*rp][2*cp]=M_P[2*rp][2*cp]+mp[2*i][2*j]
            M_P[2 * rp][2 * cp+1] = M_P[2 * rp][2*cp+1] + mp[2 * i][2 * j+1]
            M_P[2 * rp+1][2 * cp] = M_P[2 * rp+1][2 * cp] + mp[2 * i+1][2 * j]
            M_P[2 * rp+1][2 * cp+1] = M_P[2 * rp+1][2*cp+1] + mp[2 * i+1][2 * j+1]
            # print(M_P)
    # print('----------------------------------------------------')

# print(M_P)

# print(np.linalg.inv(Kg))
eval,evec=np.linalg.eig(Kg)
# print('Eigen value',eval)
# # print('Eigen vector',evec)
# # condnum=np.linalg.cond(Kg)
# # print(condnum)

# eval_m,evec_m=np.linalg.eig(M_P)
# print('Eigen value',eval_m)
# # # print('Eigen vector',evec_m)
# eval_w,evec_w=linalg.eig(Kg,M_P)
# print('Eigen value whole',eval_w)

fg = np.zeros((2*nop, 1), dtype=float)

'''Apply Boundary condition'''
# print(Ng)
Assemble_k=Kg
Assemble_m=M_P
# F=fg

"""FOR RIGID BODY TRANSLATION"""
# Values_PV=np.array([5,0,5,0])
# nodes_PV=np.array([0,1,2*part,2*part +1])

"""FOR RIGID BODY ROTATION"""
# Values_PV=np.array([0,5,5,5])
# nodes_PV=np.array([0,1,2*part,2*part +1])

"""CONSTANT CURVATURE CONDITION"""
# Values_PV=np.array([0,0,2.5,5])
# nodes_PV=np.array([0,1,2*part,2*part +1])

"""CANTELEVER BEAM CONCENTRATED LOAD AT THE FREE END"""
# Values_PV=np.array([0,0,-.3333,-.5])
# nodes_PV=np.array([0,1,2*part,2*part +1])

# Values_PV=np.array([0,0])
# nodes_PV=np.array([0,1])
# Values_SV=np.array([-1])
# nodes_SV=np.array([2*part])

Values_PV=np.array([0,0])
nodes_PV=np.array([0,2*part])
# # Values_SV=np.array([-1])
# # nodes_SV=np.array([mid])
#
J = defbc(PV_Val=Values_PV, PV_Nodes=nodes_PV)#, SV_Nodes=nodes_SV, SV_Val=Values_SV)
# J = defbc(PV_Val=Values_PV, PV_Nodes=nodes_PV, SV_Nodes=nodes_SV, SV_Val=Values_SV)
H = J.getbc()  # H contains the dictionary of dictionaries
#print(Assembled_K)
# N = [Assemble_k, F]
N = [Assemble_k, Assemble_m]
Appbc = bcapply(Assemble_k, Assemble_m, H)
L = Appbc.applybc()
print(L[0],L[1])
eval_w1,evec_w1=linalg.eig(L[0],L[1])
print('Eigen value whole1',eval_w1)

Eig_val=[]
print(np.shape([eval_w1])[1])
for i in range(np.shape([eval_w1])[1]):
    Eig_val.append(abs([eval_w1][0][i]))

print(math.pi)
Eig_val=sorted(Eig_val)
print(Eig_val)
freq=[]
for i in range(8):
    freq.append((Eig_val[i]**.5)/2*math.pi)
    # freq.append((Eig_val[i] ** .5) )
print(freq)
# fr1=((1.875)**2) *(math.sqrt(E*I/(rho*Ar*(l**4))))
# fr2=(4.694)**2*(math.sqrt(E*I/(rho*Ar*(l**4))))
# fr3=(7.855)**2*(math.sqrt(E*I/(rho*Ar*(l**4))))
# print(fr1,fr2,fr3)


# end=time.time()
#
# print(end-start)
