from definebc import definebcs as defbc
import numpy as np


class bcapply(object):
    """APPLY THE BOUNDARY CONDITIONS"""

    def __init__(self, H, F, BCD):
        self.K = H  # Contains Assembled K
        self.F = F  # Contains Assembled F
        self.BCD = BCD  # Dictionary of dictionaries returned by definebc

    def applybc(self):
        '''Apply boundary conditions by changing the K and the RHS of the assembled set of equations. Returns the
        modified K and RHS to solve. '''
        BCD = self.BCD
        Values = BCD['BCS']
        Nodes = BCD['Dofs']
        # May have different names in the algorithm/pdf file
        VSPV = Values['p_variable']  # Values of all primary variables specified
        VSSV = Values['s_variable']  # Values of all secondary variables specified
        VNBC = Values['k_values']  # Values of all Kp speficied for mixed boundary conditions
        UREF = Values['u_ref_val']  # Values of all Uref specified [VNBC and UREF are same size]

        ISPV = Nodes['pv_dofs']  # Array of Node numbers where the primary variables are speficied
        ISSV = Nodes['sv_dofs']  # Array of Node numbers where the secondary variables are  specified
        INBC = Nodes['mix_dofs']  # Array of Node numbers where the mixed boundary conditions are specified

        NSPV = VSPV.size
        NSSV = VSSV.size
        NNBC = VNBC.size

        K = self.K
        F = self.F
        nrows = K.shape[0]

        for (k, ispv) in enumerate(ISPV):
            K[ispv, ispv] = 1
            F[ispv] = VSPV[k]
            for i in range(nrows):
                if i != ispv:
                    F[i] = F[i] - K[i, ispv] * VSPV[k]
            for t in range(nrows):
                if t != ispv:
                    K[t, ispv] = 0.0
                    K[ispv, t] = 0.0

        for (k, issv) in enumerate(ISSV):
            F[issv] = F[issv] + VSSV[k]

        for (k, inbc) in enumerate(INBC):
            K[inbc, inbc] = K[inbc, inbc] + VNBC[k]
            F[inbc] = F[inbc] + VNBC[k] * UREF[k]

        return [K, F]


# if __name__ == "__main__":
#     Assembled_K = np.array([[1, -1, 0, 0], [-1, 2, -1, 0], [0, -1, 2, -1], [0, 0, -1, -1]])  # Assembled K^G
#     F = np.array([1.0, 2.0, 3.0, 4.0])  # Assembled F^G
#
#     Values = np.array([4.1, 4.2])
#     nodes = np.array([1, 3])
#     # J = defbc(PV_Val=Values, PV_Nodes=nodes, SV_Val=np.array([2]), SV_Nodes=np.array([2]), K_Val=np.array([1]),
#     #        Uref_Val=np.array([0.222]), Mix_Nodes=np.array([0]))
#     defbc()
#     J = defbc(PV_Val=Values, PV_Nodes=nodes)
#
#     Values_PV = np.array([99])
#     nodes_PV = np.array([0])
#
#     Values_SV = np.array([1000])
#     nodes_SV = np.array([3])
#
#     J = defbc(PV_Val=Values_PV, PV_Nodes=nodes_PV, SV_Nodes=nodes_SV, SV_Val=Values_SV)
#     H = J.getbc()  # H contains the dictionary of dictionaries
#     #print(Assembled_K)
#     N = [Assembled_K, F]
#     Appbc = bcapply(Assembled_K, F, H)
#     L = Appbc.applybc()
#     print("Modified KG\n", L[0])
#     print("Modified FG\n", L[1])
#
#     # Now you can solve the problem (Try solving the problem, without the boudnary conditions)
#     sol = np.linalg.solve(L[0], L[1])
#     print(sol)
