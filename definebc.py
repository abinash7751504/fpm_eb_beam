import numpy as np
# This function defines the boundary conditions in a way that can be given to the applybc class.

class definebcs(object):

    def __init__(self, PV_Val=np.array([]), SV_Val=np.array([]), K_Val=np.array([]), Uref_Val=np.array([]),
                 PV_Nodes=np.array([]), SV_Nodes=np.array([]), Mix_Nodes=np.array([])):
        self.PV_Val = PV_Val
        self.SV_Val = SV_Val
        self.K_Val = K_Val
        self.Uref_Val = Uref_Val

        self.PV_Nodes = PV_Nodes
        self.SV_Nodes = SV_Nodes
        self.Mix_Nodes = Mix_Nodes

    def getbc(self):
        """Get the bc"""
        PV_Val = self.PV_Val
        SV_Val = self.SV_Val
        K_Val = self.K_Val
        Uref_Val = self.Uref_Val

        PV_Nodes = self.PV_Nodes
        SV_Nodes = self.SV_Nodes
        Mix_Nodes = self.Mix_Nodes

        if np.size(PV_Val) != np.size(PV_Nodes):
            print(
                'Error:::::::PV_Val and PV_Nodes should have the same size. PV_Val has the values of the primary '
                'variable at the nodes and PV_Nodes has the corresponding node numbers. Program will exit.')
            exit()
        if np.size(SV_Val) != np.size(SV_Nodes):
            print(
                'Error:::::::SV_Val and SV_Nodes should have the same size. SV_Val has the values of the primary '
                'variable at the nodes and SV_Nodes has the corresponding node numbers. Program will exit.')
            exit()
        if np.size(K_Val) != np.size(Mix_Nodes) or np.size(Uref_Val) != np.size(Mix_Nodes):
            print(
                'Error:::::::K_Val, Uref_Val and Mix_Nodes should have the same size. K_Val and Uref_Val have the '
                'values '
                'of for specifying the mixed boundary conditions and Mix_Nodes has the corresponding node '
                'numbers. Program will exit.')
            exit()

        Ar1 = {'p_variable': PV_Val, 's_variable': SV_Val, 'k_values': K_Val, 'u_ref_val': Uref_Val}
        Ar2 = {'pv_dofs': PV_Nodes, 'sv_dofs': SV_Nodes, 'mix_dofs': Mix_Nodes}

        return {'BCS': Ar1, 'Dofs': Ar2} # Returns a dictionary of BCS (values) and Dofs (The degrees of freedom
        # where we will apply the boundary conditions)